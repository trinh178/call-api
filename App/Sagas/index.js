import { all, takeLatest } from 'redux-saga/effects'
import { StartupTypes } from '../Redux/StartupRedux'
import { startup } from './StartupSagas'

/* ------------ REDUX ------------ */
//@nhancv 2019-03-11
//TODO: Add REDUX here: Action, Function, Service
import { UsersType, UsersFunction } from '../Containers/Users/Users.Action'
import UsersApi from '../Containers/Users/Users.Api'

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(UsersType.REQUEST_FETCH_USER, UsersFunction.onFetchUser, UsersApi.create()),

    //@nhancv 2019-03-11
    //TODO: redux flow configuration

  ])
}
