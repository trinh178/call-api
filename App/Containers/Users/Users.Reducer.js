import { UsersType } from './Users.Action'
import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'
/* ------------- Initial State ------------- */
const INITIAL_STATE = Immutable({
  data: null,
  isFetching: false,
  error: false,
  page: 0,
  canLoadMore: false
})
/* ------------- Reducers ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [UsersType.REQUEST_FETCH_USER]: (state, action) => {
    //console.log('REQUEST_FETCH_USER', state)
    //console.log('REQUEST_FETCH_USER', action)
    return {...state,
      //data: null,
      isFetching: true,
      error: false,
      page: action.body.page,
      canLoadMore: false
    }
  },
  [UsersType.FETCH_USER_SUCCESS]: (state, action) => {
    //console.log('FETCH_USER_SUCCESS', action)
    console.log('FETCH_USER_SUCCESS', state)

    return {
      ...state,
      data: (state.page > 1)? state.data.concat(...action.data.items):action.data.items,
      isFetching: false,
      error: false,
      //page: state.page + 1,
      canLoadMore: (state.page + 1) <= Math.ceil(action.data.total_count / 5)
    }
  },
  [UsersType.FETCH_USER_FAILURE]: (state, action) => {
    console.log('FETCH_USER_FAILURE', action.error)
    return {
      ...state,
      data: null,
      isFetching: false,
      error: action.error,
      page: 1,
      canLoadMore: false
    }
  },
  [UsersType.FETCH_USER_CLEAN]: (state, action) => {
    return {
      ...state,
      data: null,
      isFetching: false,
      error: false,
      page: 1,
      canLoadMore: false
    }
  }
}) 
