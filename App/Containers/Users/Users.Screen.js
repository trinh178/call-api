import React, { Component } from 'react'
import { ActivityIndicator, View, FlatList, Text, TextInput, Button , Image} from 'react-native'
import connect from 'react-redux/es/connect/connect'
// Styles
import styles from './Users.Styles'
import { UsersAction } from './Users.Action'

class UsersScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      isRefreshing: false,
    }
    this.searchText = '';
    this.body = {
      username: 'trinh',
      page: 1
    }
  }

  render () {
    const { data, isFetching } = this.props.users
    const {isRefreshing} = this.state
    return (
      <View style={styles.mainContainer}>

        <View>
          <TextInput style={styles.search} onChangeText={(text) => {
            this.searchText = text
          }} />
          <Button title="Search" onPress={this.onSearchPress} />
        </View>

        {
          isFetching ? <ActivityIndicator size={'large'} color={'#000'}/> : null
        }

        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          automaticallyAdjustContentInsets={false}
          renderItem={this.renderItem}
          refreshing={isRefreshing}
          onEndReachedThreshold={0.5}
          ItemSeparatorComponent={this.spaceLine}
          onRefresh={this.onRefresh}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    )
  }

  renderFooter = () => {
    return (
    <View>
      {
        this.props.users.canLoadMore ? <Button title={'Load more'} onPress={this.onLoadMore}/> : null
      }
    </View>
    )
  }

  renderItem = ({ item, index }) => {
    return (
      //<View style={{ height: 30, backgroundColor: 'pink', justifyContent: 'center' }}>
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          alignItems: 'center'
        }}
        >
        <Image
          style={{width: 50, height: 50}}
          source={{uri: item.avatar_url}}
        />
        <Text style={{
          marginHorizontal: 10
        }}>{item.login}</Text>
      </View>
    )
  }

  spaceLine = () => {
    return (<View style={styles.viewUnderlineHorizontal}/>)
  }

  componentDidMount () {
    this.props.onGetUser(UsersAction.requestFetchUser(this.body))
  }

  //
  onSearchPress = () => {
    this.body = {
      username: this.searchText,
      page: 1
    }
    this.props.onGetUser(UsersAction.requestFetchUser(this.body))
  }
  onRefresh = () => {
    this.props.onGetUser(UsersAction.requestFetchUser(this.body))
  }

  onLoadMore = () => {
    if (this.props.users.canLoadMore) {
      this.body.page ++
      this.props.onGetUser(UsersAction.requestFetchUser(this.body))
    }
  }
}

function mapStateToProps (state) {
  return {
    users: state.users
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onGetUser: (request) => dispatch(request)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)(UsersScreen)
